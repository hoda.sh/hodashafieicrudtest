<?php

use App\CustomerModule\Http\CustomerController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web'], 'namespace' => 'App\CustomerModule\Http'], function () {

    Route::get('/customer', [CustomerController::class, 'index']);
    Route::post('/customer/create', [CustomerController::class, 'store'])->name('customer.store');
    Route::get('/customer/create', [CustomerController::class, 'create']);
    Route::get('/customer/{customer}', [CustomerController::class, 'show'])->name('customer.show');
    Route::put('/customer/{customer}',[CustomerController::class, 'update'])->name('customer.update');
    Route::delete('/customer/{customer}',[CustomerController::class, 'destroy']);
});
