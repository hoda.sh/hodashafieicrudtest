@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="page-header">
                    <h2>Customer Details</h2>
                </div>
                    <div class="card mb-3">
                        <div class="card-header">{{$customer->FirstName}}  {{$customer->LastName}}</div>

                        <div class="card-body">
                            PhoneNumber :
                            {{$customer->PhoneNumber}}
                            <br/>
                            DateOfBirth :
                            {{$customer->DateOfBirth}}
                            <br/>
                            Email:
                            {{$customer->Email}}
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection

