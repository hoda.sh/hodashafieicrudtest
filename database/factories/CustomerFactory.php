<?php

namespace Database\Factories;

use App\CustomerModule\Database\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'FirstName' => $this->faker->firstName,
            'LastName' => $this->faker->lastName,
            'DateOfBirth' => $this->faker->dateTime,
            'PhoneNumber' => $this->faker->numerify('###########'),
            'Email' => $this->faker->unique()->safeEmail,
            'BankAccountNumber' => $this->faker->bankAccountNumber,
        ];
    }
}
