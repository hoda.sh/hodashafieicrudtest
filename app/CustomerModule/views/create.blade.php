@extends('layouts.app')
@section('content')
    <style>
        .hide {
            display: none;
        }

        .intl-tel-input {
            width: 100%
        }

        .red {
            color: red;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><strong>Register a new Customer</strong></div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <form method="POST" action="/customer/create" id="register-form">

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" name="FirstName" placeholder="first name" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" name="LastName" placeholder="last name" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="date" class="form-control" name="DateOfBirth" placeholder="birthday" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="tel" class="form-control" name="PhoneNumber" id="phone" placeholder="" required>
                                    <input type="hidden"  name="code" id="code">
                                    <span id="error-msg" class="red hide">Invalid number</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="email" class="form-control" name="Email" placeholder="email" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="number" class="form-control" name="BankAccountNumber"
                                           placeholder="account number" required>
                                </div>
                            </div>
                            <input class="btn btn-primary" type="button" onclick="validation()" value="register">
                            <button style="display: none" class="btn btn-primary" type="submit" id="submit"></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        var telInput = $("#phone"),
            errorMsg = $("#error-msg"),
            validMsg = $("#valid-msg");

        // initialise plugin
        telInput.intlTelInput({
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
        });

        var reset = function () {
            telInput.removeClass("error");
            errorMsg.addClass("hide");
        };


        // on blur: validate
        telInput.blur(function () {
            reset();
            if ($.trim(telInput.val())) {
                if (telInput.intlTelInput("isValidNumber")) {
                    /* get code here*/
                    $('#code').val(telInput.intlTelInput('getSelectedCountryData').dialCode);

                } else {
                    telInput.addClass("error");
                    errorMsg.removeClass("hide");
                }

            }
        });

        // on keyup / change flag: reset
        telInput.on("keyup change", reset);

        function validation() {

            var el = document.getElementById('phone');

            if (!hasClass(el, 'error')) {

                $("#submit").click();
            }
        }

        function hasClass(element, cls) {
            return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
        }

    </script>
@endsection
