<?php

namespace App\CustomerModule\Http;

use App\CustomerModule\Database\Models\Customer;
use App\Http\Controllers\Controller;
use App\Rules\UniqueCustomer;
use Illuminate\Http\Request;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class CustomerController extends Controller
{

    public function index()
    {
        $customers = Customer::all();
        return view('customer::index', compact('customers'));
    }

    public function create()
    {
        return view('customer::create');
    }

    public function store(Request $request)
    {
        $phoneUtil = PhoneNumberUtil::getInstance();

        try {

            $this->validate($request, [
                'FirstName' => ['required', new UniqueCustomer($request->lastName, $request->birthday)],
                'LastName' => 'required',
                'DateOfBirth' => 'required',
                'PhoneNumber' => 'required',
                'Email' => 'required|string|email|max:255|unique:customers',
                'BankAccountNumber' => 'required',
            ]);

            $swissNumberProto = $phoneUtil->parse($request->phone, "US");

            if (!$phoneUtil->isValidNumber($swissNumberProto))
                return 'invalid phone number';

            $swissNumberProto = $phoneUtil->format($swissNumberProto, PhoneNumberFormat::E164);

            Customer::create([
                'FirstName' => $request->FirstName,
                'LastName' => $request->LastName,
                'DateOfBirth' => $request->DateOfBirth,
                'PhoneNumber' => $swissNumberProto,
                'Email' => $request->Email,
                'BankAccountNumber' => $request->BankAccountNumber,
            ]);

            return redirect('/customer/' . $request->email);

        } catch (NumberParseException $e) {

            return $e;
        }

    }


    public function show($email)
    {
        $customer = Customer::where('Email', $email)->first();

        return view('customer::show', compact('customer'));
    }


    public function edit(Customer $customer)
    {
        //
    }


    public function update(Request $request, $email)
    {
        $customer = Customer::where('Email', $email)
            ->update([
            'FirstName' => $request->FirstName,
            'LastName' => $request->LastName,
            'DateOfBirth' => $request->DateOfBirth,
            'PhoneNumber' => $request->PhoneNumber,
            'Email' => $request->Email,
            'BankAccountNumber' => $request->BankAccountNumber,
        ]);

        return redirect('/customer/' . $customer->Email);
    }


    public function destroy($email)
    {

        Customer::where('Email', $email)->delete();

        return redirect("/customer");
    }

}
