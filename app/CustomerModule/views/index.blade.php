@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="page-header">
                    <h2>All Customers</h2>
                </div>
                @foreach($customers as $customer)
                    <div class="card mb-3">
                        <div class="card-header">{{$customer->FirstName}}  {{$customer->LastName}}</div>

                        <div class="card-body">
                            <a href="{{route('customer.show',$customer->Email)}}">show details</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
