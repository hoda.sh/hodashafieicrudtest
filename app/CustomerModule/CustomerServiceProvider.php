<?php

namespace App\CustomerModule;

use Illuminate\Database\Eloquent\Factories\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;

class CustomerServiceProvider extends ServiceProvider
{
    public function register()
    {
        // $this->mergeConfigFrom(__DIR__.'/config/customer_config.php', 'customer');
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');
        $this->loadRoutesFrom(__DIR__.'/customer_routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'customer');

    }

}
