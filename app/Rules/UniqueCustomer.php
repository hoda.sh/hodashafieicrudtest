<?php

namespace App\Rules;

use App\CustomerModule\Database\Models\Customer;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class UniqueCustomer implements Rule
{

    protected $lastName;
    protected $birthday;

    public function __construct($lastName, $birthday)
    {
        $this->lastName = $lastName;
        $this->birthday = $birthday;
    }

    public function passes($attribute, $value)
    {
        $count = Customer::where('FirstName', $value)
            ->where('LastName', $this->lastName)
            ->where('DateOfBirth', $this->birthday)
            ->count();

        return $count === 0;
    }

    public function message()
    {
        return 'this customer already exist';
    }
}
