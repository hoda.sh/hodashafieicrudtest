<?php

namespace App\CustomerModule\tests\Feature;

use App\CustomerModule\Database\Models\Customer;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    use DatabaseTransactions;

    private $customer;

    public function setUp(): void
    {
        parent::setUp();

        $this->customer = Customer::factory()->create();

    }

    /** @test */
    public function a_user_can_read_all_customers()
    {
        //Given we have customer in the database => $this->customer

        //When user visit the customers page
        $response = $this->get('/customer'); // your route to get customer

        //Then should be able to see the customers
        $response->assertSee( $this->customer->FirstName);
    }

    /** @test */
    public function a_user_can_read_single_customer()
    {
        //Given we have customer in the database => $this->customer

        //When user visit the custom's URI
        $response = $this->get('/customer/' .  $this->customer->Email);

        //Then can see the task details
        $response->assertSee($this->customer->FirstName1)
            ->assertSee($this->customer->PhoneNumber);
    }

    /** @test */
    public function a_user_can_create_a_new_customer()
    {
        //Given we have a Customer object => $this->customer

        //When user submits post request to create customer endpoint
        $this->post('/tasks/create', $this->customer->toArray());

        //It gets stored in the database
        $this->assertEquals(1, Customer::where('Email', $this->customer->Email)->count());

    }

    /** @test */
    public function a_customer_requires_a_firstName(){

        //Given we have't Customer's FirstName
        $customer = Customer::factory()->make(['FirstName' => null]);

        //When user submits post request to create customer endpoint
        $this->post('/customer/create',$customer->toArray())

            //It gets an error
            ->assertStatus(302);
    }

    /** @test */
    public function a_customer_requires_a_LastName(){

        //Given we have't Customer's LastName
        $customer = Customer::factory()->make(['LastName' => null]);

        //When user submits post request to create customer endpoint
        $this->post('/customer/create',$customer->toArray())
            //It gets an error
            ->assertStatus(302);
    }

    /** @test */
    public function a_customer_requires_a_DateOfBirth(){

        //Given we have't Customer's DateOfBirth
        $customer = Customer::factory()->make(['DateOfBirth' => null]);

        //When user submits post request to create customer endpoint
        $this->post('/customer/create',$customer->toArray())
            //It gets an error
            ->assertStatus(302);
    }

    /** @test */
    public function a_customer_requires_a_PhoneNumber(){

        //Given we have't Customer's PhoneNumber
        $customer = Customer::factory()->make(['PhoneNumber' => null]);

        //When user submits post request to create customer endpoint
        $this->post('/customer/create',$customer->toArray())
            //It gets an error
            ->assertStatus(302);
    }

    /** @test */
    public function a_user_can_update_the_customer(){

        $this->customer->FirstName = "Updated FirstName";

        //When the user hit's the endpoint to update the customer
        $this->put('/customer/'.$this->customer->Email, $this->customer->toArray());

       //The customer should be updated in the database.
        $this->assertDatabaseHas('customers',['Email'=> $this->customer->Email , 'FirstName' => 'Updated FirstName']);

    }

    /** @test */
    public function a_user_can_delete_the_customer(){

        //Given we have a customer

        //When the user hit's the endpoint to delete the customer
        $this->delete('/customer/'.$this->customer->Email);

        //The customer should be deleted from the database.
        $this->assertDatabaseMissing('customers',['Email'=> $this->customer->Email]);

    }

}
