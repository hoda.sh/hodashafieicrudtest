<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('DateOfBirth');
            $table->string('PhoneNumber',15);
            $table->string('Email')->unique();
            $table->string('BankAccountNumber');
            $table->primary(['FirstName','LastName','DateOfBirth']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
